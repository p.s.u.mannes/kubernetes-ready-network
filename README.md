# Vagrantfile to set up a minimal private network for kubernetes
Hosts...
- reach the internet via NIC 1
- communicate with eachother via NIC 2
- are named master, node01, node02, ...
- have DNS stored in /etc/hosts
- additional steps to run k8s will be necessary, this simply sets up
bare-bone hosts with CRI-O on a private network and configures DNS.
- Additional steps include e.g.
disabling swap, additional networking config, steps to bootstrap a cluster

## Prerequisites
1. Vagrant
2. 8+ Gig RAM
3. Virtualbox

Create/edit /etc/vbox/networks.conf and add the following:
```
* 0.0.0.0/0 ::/0
```

Then host only networks can be in any range, see:
https://discuss.hashicorp.com/t/vagrant-2-2-18-osx-11-6-cannot-create-private-network/30984/23

